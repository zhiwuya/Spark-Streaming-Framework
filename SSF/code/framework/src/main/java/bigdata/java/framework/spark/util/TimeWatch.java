/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util;

import java.util.HashMap;
import java.util.Map;

/**
 * 执行时间统计类
 */
public class TimeWatch {
    /** 构造函数
     * @param openCalTimestamp 设置是否执行统计时间(true是，false否)
     */
    public TimeWatch(Boolean openCalTimestamp)
    {
        this.openCalTimestamp = openCalTimestamp;
    }

    /**
     * 构造函数
     */
    public TimeWatch()
    {

    }

    /**
     * 设置是否执行统计时间
     * @param openCalTimestamp true是，false否
     */
    public void setOpenCalTimestamp(Boolean openCalTimestamp)
    {
        this.openCalTimestamp = openCalTimestamp;
    }
    //是否开启计时统计执行时间
    Boolean openCalTimestamp=true;
    Map<String,Long> timestampMap = new HashMap<>();
    /**
     * 计算某个逻辑执行时间
     * @param groupName 组名称
     * @param task 执行逻辑
     * @return 返回当前逻辑执行耗时
     */
    public Long doFunction(String groupName, WatchFunction task)
    {
        Long time =0L;
        if(openCalTimestamp)
        {
            long startTime = System.currentTimeMillis();
            task.call();
            time = System.currentTimeMillis() - startTime;
            Long aLong = timestampMap.get(groupName);
            if (aLong == null) {
                aLong = time;
            } else {
                aLong = aLong + time;
            }
            timestampMap.put(groupName, aLong);
            return aLong;
        }
        else
        {
            task.call();
        }
        return time;
    }
    /**
     *获取计时器中所有耗时Map
     */
    public Map<String,Long> getTimestamp()
    {
        return timestampMap;
    }

    /**
     *打印计时器中所有耗时
     */
    public void printTimestamp()
    {
        System.out.println(timestampMap);
    }
}
