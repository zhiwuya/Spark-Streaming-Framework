package bigdata.java.platform.controller;

import bigdata.java.platform.beans.Resoult;
import bigdata.java.platform.beans.SysSet;
import bigdata.java.platform.beans.TTask;
import bigdata.java.platform.service.TaskLogService;
import bigdata.java.platform.service.TaskService;
import bigdata.java.platform.util.Comm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/api/task/")
public class WebApiController {

    @Autowired
    TaskService taskService;

    @Autowired
    SysSet sysSet;

    @Autowired
    TaskLogService taskLogService;

    @PostMapping(value = "start")
    @ResponseBody
    public Resoult getStartTask(String appname,String token)
    {
        TTask task=valToken(appname,token);
        if(task==null)
        {
            return Resoult.fail().setMsg("token错误或任务不存在");
        }

        if(task.getStatus()!= TTask.STOPED)
        {
            return Resoult.fail().setMsg("必须是停止状态的任务才可以启动");
        }
        Date date = new Date();
        taskService.setStatusAndClearErrCountAndClearTryCount(task.getTaskId(),21,date);
        return Resoult.success().setMsg("提交成功");
    }

    @PostMapping(value = "stop")
    @ResponseBody
    public Resoult getStopTask(String appname,Boolean kill,String token)
    {
        TTask task=valToken(appname,token);
        if(task==null)
        {
            return Resoult.fail().setMsg("token错误或任务不存在");
        }

        if(task.getStatus()!= TTask.STARTED)
        {
            return Resoult.fail().setMsg("必须是已启动状态的任务才可以停止");
        }
        if(kill)
        {//强杀
            Boolean killStatus = Comm.kill(task, sysSet, taskLogService);
            if(killStatus)
            {
                return Resoult.success().setMsg("提交成功");
            }
            else
            {
                return Resoult.fail().setMsg("提交失败");
            }
        }
        else
        {//优雅关闭
            Date date = new Date();
            taskService.setStatusAndClearErrCountAndClearTryCount(task.getTaskId(),11,date);
            return Resoult.success().setMsg("提交成功");
        }
    }

    @PostMapping(value = "status")
    @ResponseBody
    public Resoult getTaskStatus(String appname,String token)
    {
        TTask task=valToken(appname,token);
        if(task==null)
        {
            return Resoult.fail().setMsg("token错误或任务不存在");
        }
        Map<String,Object> taskMap = new HashMap<>();
        taskMap.put("status",task.getStatus());
        Resoult success = Resoult.success();
        success.setData(taskMap);
        return success;
    }

    private TTask valToken(String appName,String token)
    {
        TTask tTask = taskService.getByAppName(appName);
        if(tTask ==null)
        {
            return null;
        }
        if(token.equals(tTask.getWebApiToken()))
        {
            return tTask;
        }
        return null;
    }
}
