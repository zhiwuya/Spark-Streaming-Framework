package bigdata.java.platform.service;

import bigdata.java.platform.beans.TFile;

import java.util.List;

public interface FileService {
    Boolean add(TFile tFile);
    Boolean delete(Integer fileId);
    List<TFile> list(Integer userId);
    TFile get(Integer fileId);
}
