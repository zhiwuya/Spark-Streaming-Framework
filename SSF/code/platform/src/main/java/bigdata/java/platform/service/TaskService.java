package bigdata.java.platform.service;

import bigdata.java.platform.beans.KafkaOffset;
import bigdata.java.platform.beans.TTask;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TaskService {
    TTask getDefault(Integer userId);
    TTask get(Integer taskId);
    TTask get(Integer userId,Integer taskId);
    void updateQueueName(Integer userId,String queueName);
    List<TTask> list(Integer userId);
    List<TTask> list();
    List<TTask> listByStatus(Integer userId,Integer status);
    Boolean delete(Integer taskId);
    Boolean edit(TTask tTask);
    Boolean add(TTask tTask);
    Boolean setStatus(Integer taskId, Integer status, Date date);
    Boolean updateStartTime(Integer taskId,Date startTime);
    Boolean updateErrCountAndTryCount(Integer taskId, Integer errCount,Integer tryCount, Date date);
    Boolean setStatusAndClearErrCountAndClearTryCount(Integer taskId, Integer status, Date date);
    Boolean setStatusAndClearErrCountAndSetTryCount(Integer taskId, Integer status,Integer tryCount, Date updateTime);
    Boolean updateErrCount(Integer taskId, Integer errCount, Date updateTime);
    Boolean updateErrCount(Integer taskId,Integer errCount);
    List<Map<String, Object>> getTotalMetrics(Integer userId);
    Map<String, Object> getTotalNumberMetrics(Integer userId);
    List<Map<String, Object>> getWaitingBatches(Integer userId);
    void clearAllTaskErrCountAndClearTryCount();
    List<KafkaOffset> getOffset(Integer userId,Integer taskId);
    Boolean updateOffSet(Integer taskId,String topic,String partition,String currentOffset,String min,String max);
    void stopByMarkKey_Mysql(String appname);
    void stopByMarkKey_Hbase(String appname);
    void deleteStopbyMark_Mysql(String appName);
    void deleteStopbyMark_Hbase(String appName);
    TTask getByAppName(String appName);
}
