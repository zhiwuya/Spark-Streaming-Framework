package bigdata.java.platform.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 定时器工具类
 */
public class TimerUtil {
    static int count = 0;

    /**
     * 表示一天的毫秒数
     */
    public static final Long DAYMILLISECONDS= 86400000L;

    /**
     * 固定每天几点执行事件
     * @param task TimerTask
     * @param hourOfDay 小时，24小时制
     */
    public static void timerEveryDay(TimerTask task,Integer hourOfDay) {
        timer(task, hourOfDay,00,00,DAYMILLISECONDS);
    }

    /**
     * 固定每天几点几分执行事件
     * @param task TimerTask
     * @param hourOfDay 小时，24小时制
     * @param minute 分钟数
     */
    public static void timerEveryDay(TimerTask task,Integer hourOfDay, Integer minute) {
        timer(task, hourOfDay,minute,00,DAYMILLISECONDS);
    }

    /**
     * 固定每天时，分，秒执行事件
     * @param task TimerTask
     * @param hourOfDay 小时，24小时制
     * @param minute 分钟
     * @param second 秒
     */
    public static void timerEveryDay(TimerTask task,Integer hourOfDay, Integer minute,Integer second) {
        timer(task, hourOfDay,minute,second,DAYMILLISECONDS);
    }
    /**
     * 固定每天指定时，分，秒执行事件(重复或不重复)
     * @param task TimerTask
     * @param hourOfDay 小时，24小时制
     * @param minute 分钟
     * @param second 秒
     * @param period 每隔多少毫秒重复执行事件。如果为null仅执行一次，后面不再重复执行。
     */
    public static void timer(TimerTask task,Integer hourOfDay, Integer minute,Integer second,Long period) {
        //设置执行时间
        Calendar calendar = Calendar.getInstance(DateUtil.timeZone);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH );
        int day = calendar.get(Calendar.DAY_OF_MONTH);//每天
        //指定每天的21:09:00执行，
        calendar.set(year, month, day, hourOfDay, minute, second);
        Date date = calendar.getTime();
        Timer timer = new Timer();
//        System.out.println(date);
        if(period!=null)
        {
//            int period = 2 * 1000;
            //每天的date时刻执行task，每隔period秒重复执行，一直不停止
             timer.scheduleAtFixedRate(task, date, period);
        }
        else
        {
            //今天date时刻执行task, 仅执行一次
            timer.schedule(task, date);
        }
    }

    /**
     * 忽略
     */
    public static void main(String[] args) {
        timer(new TimerTask() {
            @Override
            public void run() {
                count++;
                System.out.println(Thread.currentThread().getName()+",时间=" + new Date() + " 执行了" + count + "次"); // 1次
            }
        },00,05,00,3600000L);

//        RecurringTimer recurringTimer = new RecurringTimer()

        //每天00点05分，删除前一天的数据
//        TimerUtil.timer(new TimerTask() {
//            @Override
//            public void run() {
//                String formateTime = DateUtil.getFormateTime(FORMAT_YYYY_MM_DDHHMMSS);
//                String sql = "delete from REALTIME_JFXQ where PROCESSDATE <  trunc(sysdate)";
//                OracleUtil.getInstance().ExecuteSQL(sql);
//                count++;
//                System.out.println(count + "," +Thread.currentThread().getName() + ",time = "+formateTime + " , sql = "+sql);
//            }
//        },00,05,00,20000L);
    }
}
