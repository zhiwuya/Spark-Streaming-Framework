package bigdata.java.platform.service;

import bigdata.java.platform.beans.UserInfo;

import java.util.List;

public interface UserService {
    UserInfo doLogin(String userName,String passWord);

    Boolean modifyPwd(String userName,String newPassWord);

    Boolean add(UserInfo userInfo);
    Boolean edit(UserInfo userInfo);
    Boolean delete(Integer userId);
    UserInfo get(Integer userId);
    List<UserInfo> list();

}
