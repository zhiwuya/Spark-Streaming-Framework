
//左侧导航菜单效果
$(function() {
	$('.d-firstNav').click(function(e) {
		dropSwift($(this), '.d-firstDrop');	   
		e.stopPropagation();
	});
	$('.d-secondNav').click(function(e) {
		dropSwift($(this), '.d-secondDrop');
		e.stopPropagation();
	});
	
	
	
	/**
	 * @param dom   点击的当前元素
	 * @param drop  下一级菜单
	 */
	function dropSwift(dom, drop) {
		//点击当前元素，收起或者伸展下一级菜单
		
		
		dom.next().slideToggle();
		
		//设置旋转效果
		
		//1.将所有的元素都至为初始的状态		
		dom.parent().siblings().find('.fa-caret-right').removeClass('iconRotate');
		
		//2.点击该层，将其他显示的下滑层隐藏		
		dom.parent().siblings().find(drop).slideUp();
		
		var iconChevron = dom.find('.fa-caret-right');
		if(iconChevron.hasClass('iconRotate')) {			
			iconChevron.removeClass('iconRotate');
		} else {
			iconChevron.addClass('iconRotate');
		}
		//alert("aaa");
		//$('#page_LeftMain').getNiceScroll().resize();
		
	}
})

//给左侧导航加选中状态，并切把A赋值给框架
 $(document).ready(function() {
	 var $Ifram=$("#mainFrame");
	 //给没有下拉菜单的一级菜单加选中。并把链接赋值给框架
	 $(".s-firstItem").eq(0).addClass("l1_select_dq")
	 $(".s-firstItem").click(function(){
		 var $ahref=$(this).children("a").attr("href");
		 //alert($ahref)
		 $Ifram.attr("src", $ahref);
		 $(this).addClass("l1_select_dq");
		 $(this).siblings().removeClass('l1_select_dq');
		 $(".s-thirdItem").removeClass("currentl3");
		 $(".s-thirdItem").removeClass("currentl3");
		 $(this).siblings(".first").find(".d-firstDrop").hide()//点击
	 })
	 //给没有下拉菜单的二级菜单加选中。并把链接赋值给框架
	  $(".s-secondItem").click(function () {
			var $l2Ahref=$(this).children("a").attr("href");
			var $l2A=$(".l2").children("a");
			var $sslist = $(this).next(".sslist");
           $(".s-thirdItem").removeClass("currentl3");
           $(".s-secondItem").removeClass("currentl2");			
           $(this).addClass("currentl2");
		   $Ifram.attr("src", $l2Ahref);
		   $(".s-firstItem").removeClass("l1_select_dq");			
       });
	 
	 //给三级菜单加选中。并把链接赋值给框架
	 $(".s-thirdItem").click(function () {
		//alert($(".s-side").height());
		var $l3Ahref=$(this).children("a").attr("href");
		var $l3A=$(".s-thirdItem").children("a");
		$l3A.attr('disabled',"true")
		//alert($l3Ahref)
		$(".s-thirdItem").removeClass("currentl3");
		$(this).addClass("currentl3");
		
		$(".s-secondItem").removeClass("currentl2");
		$(".s-firstItem").removeClass("l1_select_dq");
		if($l3Ahref==false)
		{
			return false;
		}
		else{
			$Ifram.attr("src", $l3Ahref);
			}
	});
 });
//end给左侧导航加选中状态，并切把A赋值给框架


//设置右侧框架的高度
$(function () {
	layoutrezise();
	AutoHeight();
})
	function layoutrezise() {
	var headerH = $("div#page_top").height(); //获取头部的高度
	var bodyerH = $(window).height() - headerH;  //计算内容的高度（不包括头部）
	$("div#page_RtMain,div#page_IframeMain").height(bodyerH); //设置主框架的高度
	$("#mainFrame").css('height',bodyerH-5);
	//$(".page_RtMain").width($(window).width()-230)
}
function AutoHeight() {
	$(window).resize(function () {
		layoutrezise();
	})
}  
//end设置右侧框架的高度


//当左侧导航超过页面，左侧导航有美化的滚动条
//获取左侧导航的高度
$(function () {
	leftAutoHeight();
	$(window).resize(function() {
	  leftAutoHeight()
	});
	//初始化美化滚动条
	$("#page_LeftMain").mCustomScrollbar({
		theme:"dark-thick", //主题颜色
		scrollButtons:{
			enable:false //是否使用上下滚动按钮
		},
		autoHideScrollbar: true, //是否自动隐藏滚动条
		scrollInertia :0,//滚动延迟
		horizontalScroll : false,//水平滚动条
		//mouseWheel:false //禁用鼠标的滚轮
	});
	//end初始化美化滚动条
})
//设置左侧导航的高度
function leftAutoHeight()
{
	var bodyHight=$(window).height();
	var leftMainH=bodyHight-60;
	$("#page_LeftMain").height(leftMainH);
	$("#page_LeftMain").css("background","#38495b");
}
//end当左侧导航超过页面，左侧导航有美化的滚动条