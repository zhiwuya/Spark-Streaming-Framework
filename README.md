# Spark Streaming Framework简称SSF（一个完善的Spark Streaming二次封装开源框架，包含：kafka偏移量管理，web后台管理，web api启动、停止spark streaming，宕机告警、自动重启等等功能支持，用户只需要关心业务代码，无需关注繁琐的技术细节，大大提高实时流开发效率和难度。）

#### 项目介绍
基于Spark Streaming的大数据实时流计算平台和框架（包括：调度平台，开发框架，开发demo）

  大数据实时流计算框架Spark Streaming是比较流程的框架，但是目前很少有针对Spark Streaming封装的开源平台项目，本项目是基于Spark Streaming + Springboot开发的一套完成的开发和调度平台，为了降低对Spark Streaming使用难度（如：界面化提交任务，优雅关闭程序，kafka管理偏移量，监控告警，自动复活重启等），该项目在企业内部稳定运行超过一年，现将整套代码开源，企业或者个人使用这个框架可以降低开发spark streaming的难度，用户可是甚至不用关心怎么读取kafka数据，管理偏移量，spark streaming的程序如何启动，停止，把目前遇到的坑都给解决了，只需要编写实时流计算的业务代码就可以了（只要写java/scala的业务代码或者算子即可）。希望对大家有帮助。本人精力有限，希望可以认识更多喜欢Spark Streaming的朋友，一起维护该项目。主要分为3个项目，均是用ide开发的。

开发工具使用的是Idea，hdfs，spark，hbase，livy等组件可以根据自己集群的版本进行修改pom文件。

- platform：web ui 后台管理工具，主要负责spark streaming任务提交，调度、监控，启停，job管理，参数设置的等等，这个要打包成war文件，部署在tomcat中。
- framework：开发框架，很多功能都封装在这个jar包中，大部分情况下这个里面的代码是不需要修改的，只需要用maven install到本地仓库即可，spark streaming中代码不需要再管理kafka偏移量，读取指定kafka的topic，和一些连接池工具类等等。
- bigdata：用来开发实时流业务代码的项目骨架，你代码都是在这个骨架中开发。

QQ交流群：858966066，使用过程中有什么疑问可以互相交流

gitee地址：https://gitee.com/EA89B45B220E/Spark-Streaming-Framework

#### 功能介绍
语言：支持java/scala

功能：
- 依赖管理：程序打包时jar/config是不需要讲依赖包打进去的,可以在platform中上传依赖的jar/config(将会存储在hdfs),应用程序启动时,会自动通过livy加载jar/config的参数进行提交。
- Job版本管理：每当你的业务代码，spark streaming逻辑有变更时，上传最新的job包，历史的版本也可以查看到和选择执行历史版本。
- 程序启动：通过在platform后台可以启动任务。
- 程序停止：通过platform后台可以停止任务；（停止包括2种，一种是普通的停止，这种是spark streaming优雅关闭，一种是强杀，这种是使用yarn命令进行强杀app）
- 宕机自动重启：当你提交的spark streaming程序被人杀了，或者应用程序本身的问题挂了，platform会监控这个任务，自动重启，一个任务每天最多重启3次。
- 宕机告警短信：spark streaming程序挂了，或者被杀了，会有短信提醒，后台在创建用户是配置手机号码，发送短信的api需要自己实现。
- 指定队列运行：指定yarn队列运行你的app。
- 指定参数运行：app启动时需要读取指定的args参数是可以在platform中配置。
- 数据反压管理：spark streaming的反压可以在platform创建任务时在args中配置，如果不配置将读取config的默认值（100）
- Elasticsearch连接池：已经实现es连接池，只需要修改配置文件指定参数即可。
- Hbase连接池：已经实现hbase连接池，只需要修改配置文件指定参数即可。
- Mysql连接池：已经实现Mysql连接池，只需要修改配置文件指定参数即可。
- Oracle连接池：已经实现oracle连接池，只需要修改配置文件指定参数即可。
- Redis连接池：已经实现redis连接池，只需要修改配置文件指定参数即可。
- Kafka生产者连接池：已经实现Kafka连接池，只需要修改配置文件指定参数即可。
- 数据处理统计情况：支持查看kafka的offset消费情况，剩余多少数据没有消费。
- 数据积压告警：在创建任务时可以指定积压批次的数量就进行短信告警。
- kafka偏移量管理：可在platform中指定offset从特定位置进行消费数据。
- 统计：通过图标的方式显示最近7天app消费和kafka生成的数据情况。
- log下载：通过调用ssh，可以下载Livy和Yarn的运行日志。
- Web api功能：使用http请求启动，停止，强杀，获取状态来调度你的app程序。

#### 部署
参考：Spark-Streaming-Framework/SSF/部署相关/部署指南.txt 或者加群咨询.QQ交流群：858966066

界面截图：
![首页](https://images.gitee.com/uploads/images/2019/1021/124958_b860fbe3_1548175.png "20191021124830.png")
![系统设置](https://images.gitee.com/uploads/images/2019/1021/125130_8a35069a_1548175.png "20191021125049.png")
![job管理](https://images.gitee.com/uploads/images/2019/1021/125220_f3ecb9d2_1548175.png "20191021125159.png")
![任务管理](https://images.gitee.com/uploads/images/2019/1021/125323_6f0746df_1548175.png "20191021125239.png")
![新建任务](https://images.gitee.com/uploads/images/2019/1021/125431_eec10adc_1548175.png "20191021125345.png")
其他更多功能需要不提供截图，可以自己去看。